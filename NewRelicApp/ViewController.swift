//
//  ViewController.swift
//  NewRelicApp
//
//  Created by Feizal Badri Asmoro on 9/4/17.
//  Copyright © 2017 Feizal Badri Asmoro. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UITableViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerButton.setTitle("Loading...", for: .disabled)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    @IBAction func didTapRegister(_ sender: Any) {
        registerButton.isEnabled = false

        let parameters: Parameters = [
            "email": emailTextField.text ?? "",
            "password": passwordTextField.text ?? ""
        ]
        
        Alamofire.request("http://0.0.0.0:3000/register",
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default)
            .responseJSON { response in
                self.registerButton.isEnabled = true
                
                let statusCode = response.response!.statusCode

                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    var title = ""
                    var message = ""

                    switch statusCode {
                        case 200:
                            title = "Register Success"
                            break
                        case 500:
                            title = JSON["error"] as! String
                            message = JSON["message"] as! String
                            break
                        default:
                            break
                    }
                    
                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    let close = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alert.addAction(close)
                    self.present(alert, animated: true, completion: nil)
                }
            }
    }
}
