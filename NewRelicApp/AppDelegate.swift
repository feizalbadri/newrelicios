//
//  AppDelegate.swift
//  NewRelicApp
//
//  Created by Feizal Badri Asmoro on 9/4/17.
//  Copyright © 2017 Feizal Badri Asmoro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NewRelic.start(withApplicationToken:"AA8e1664b2af01556f05b76b0c09ab21152b0f7217")
        return true
    }

}

